# Sprint Retrospective

###### Created by Elizabeth Williams & Candis Bross

## Instruction to Run

- In Visual Studio, open the SQL Server Object Explorer
- Right click on "SQL Server" and select "Add SQL Server..."
- Create a new database called "SprintRetrospective"
- Once created, right click on your new database and select "Properties" and find the Connection String under "General", copy this string
- Create or edit your appsettings.json file and copy the Connection String from the last step into this file beside "DefaultConnection"
- Your project should now be ready to run via IIS Express
- Upon the first run of the application, your database will be populated with test data
