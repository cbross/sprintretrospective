﻿using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class UserProjectModel
    {
        private AppDbContext _db;
        public UserProjectModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<UserProject> GetAll()
        {
            return _db.UserProjects.ToList<UserProject>();
        }
        // Get all users for a project
        public List<User> GetAllUsers(int id)
        {
            var userProjects = _db.UserProjects.Where(up => up.Project.Id == id).ToList();

            List<User> users = new List<User>();
            foreach (UserProject up in userProjects)
            {
                User projectUser = _db.Users.Where(user => user.Id == up.User.Id).FirstOrDefault<User>();
                users.Add(projectUser);
            }
            return users;
        }
    }
}