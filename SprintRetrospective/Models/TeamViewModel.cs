﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SprintRetrospective.Models
{
    public class TeamViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<Team> Teams { get; set; }
        public List<User> Users { get; set; }
        public List<User> AllUsers { get; set; }
        public IEnumerable<SelectListItem> GetTeams()
        {
            return Teams.Select(team => new SelectListItem { Text = team.Name, Value = team.Id.ToString() });
        }

        public IEnumerable<SelectListItem> GetAllUsers()
        {
            return AllUsers.Select(user => new SelectListItem { Text = user.FirstName + " " + user.LastName, Value = user.Id.ToString() });
        }

        public IEnumerable<SelectListItem> GetTeamUsers()
        {
            return Users.Select(user => new SelectListItem { Text = user.FirstName + " " + user.LastName, Value = user.Id.ToString() });
        }
    }
}
