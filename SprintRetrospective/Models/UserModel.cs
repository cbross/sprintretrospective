﻿using Microsoft.AspNetCore.Http;
using SprintRetrospective.Utils;
using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class UserModel
    {
        private AppDbContext _db;
        public UserModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public User Get(string id)
        {
            return _db.Users.Where(user => user.Id == id).FirstOrDefault();
        }
        public List<User> GetAll()
        {
            return _db.Users.ToList<User>();
        }

        public List<Team> GetAllTeams()
        {
            List<Team> teams = (from team in _db.Teams
                                select team).ToList<Team>();

            return teams;
        }

        public User GetLoggedInUser()
        {
            string username = SessionVars.User;
            User user = _db.Users.First(u => u.UserName == username);
            return user;
        }
    }
}