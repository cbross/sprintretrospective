﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SprintRetrospective.Models
{
    public class TeamModel
    {
        private AppDbContext _db;
        public TeamModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Team> GetAll()
        {
            return _db.Teams.ToList<Team>();
        }
        public Team Get(int id)
        {
            return _db.Teams.Where(team => team.Id == id).FirstOrDefault();
        }
        public List<User> GetUsers(int teamId)
        {
            List<User> users = (from user in _db.Users
                                join userteam in _db.UserTeams on user.Id equals userteam.UserId
                                where userteam.TeamId == teamId
                                select user).ToList<User>();
            return users;
        }

        public List<User> GetAllUsers()
        {
            List<User> users = _db.Users.ToList<User>();
            return users;
        }

        public UserTeam GetUserTeam(Team team, User user, string userId)
        {
            return (UserTeam)(from ut in _db.UserTeams
                              where ut.TeamId == team.Id && ut.UserId == user.Id
                              select ut).FirstOrDefault();
        }
    }
}
