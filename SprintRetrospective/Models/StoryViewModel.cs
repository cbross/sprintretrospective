﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class StoryViewModel
    {
        public StoryViewModel() { }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int RelativeEstimate { get; set; }
        public int HoursWorked { get; set; }
        public int ProjectId { get; set; }
        public List<Task> Tasks { get; set; }
        public User UserAssigned { get; set; }
        public string Status { get; set; }
    }
}