﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class ProjectViewModel
    {
        [Required(ErrorMessage = "Project name is required")]
        public string ProjectName { get; set; }
        [Required(ErrorMessage = "Link to Pivotal Tracker is required")]
        public string PivotalTrackerLink { get; set; }
        [Required(ErrorMessage = "Initial velocity is required")]
        public int InitialVelocity { get; set; }
        [Required(ErrorMessage = "Start Date is required")]
        public DateTime StartDate { get; set; }
        [Required(ErrorMessage = "Hours for each story point is required")]
        public int HoursPerStoryPoint { get; set; }
        public int Id { get; set; }
        public Project Project { get; set; }
        public List<Project> Projects { get; set; }
        public List<User> Users { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        public List<Team> Teams { get; set; }   // To be used to display all the teams so that when creating a project one can be chosen
        public IEnumerable<SelectListItem> GetProjects()
        {
            return Projects.Select(project => new SelectListItem { Text = project.Name, Value = project.Id.ToString() });
        }

        public IEnumerable<SelectListItem> GetTeams()
        {
            return Teams.Select(team => new SelectListItem { Text = team.Name, Value = team.Id.ToString() });
        }
    }
}