﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SprintRetrospective.Models
{
    public class SprintStory
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int SprintId { get; set; }
        public int StoryId { get; set; }

        [ForeignKey("SprintId")]
        public Sprint Sprint { get; set; } // generates FK

        [ForeignKey("StoryId")]
        public Story Story { get; set; } // generates FK

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timer { get; set; }
    }
}