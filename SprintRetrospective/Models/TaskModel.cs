﻿using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class TaskModel
    {
        private AppDbContext _db;
        public TaskModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Task> GetAllByStory(int storyId)
        {
            return _db.Tasks.Where(t => t.StoryId == storyId).ToList<Task>();
        }
    }
}