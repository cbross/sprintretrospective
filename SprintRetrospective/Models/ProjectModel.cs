﻿using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class ProjectModel
    {
        private AppDbContext _db;
        public ProjectModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<Project> GetAll()
        {
            return _db.Projects.ToList<Project>();
        }
        public Project Get(int id)
        {
            return _db.Projects.Where(project => project.Id == id).FirstOrDefault();
        }
    }
}