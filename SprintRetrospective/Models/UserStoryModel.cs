﻿using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class UserStoryModel
    {
        private AppDbContext _db;
        public UserStoryModel(AppDbContext ctx)
        {
            _db = ctx;
        }
        public List<User> GetAllByStory(int storyId)
        {
            var userStories = _db.UserStories.Where(us => us.StoryId == storyId).ToList<UserStory>();
            List<User> users = new List<User>();
            foreach(UserStory us in userStories)
                users.Add(_db.Users.Where(u => u.Id == us.UserId).FirstOrDefault());

            return users;
        }
    }
}