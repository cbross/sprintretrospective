﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class SprintModel
    {
        private AppDbContext _db;
        public SprintModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public Sprint Get(int projectId)
        {
            return _db.Sprints.Where(s => s.ProjectId == projectId && s.StartDate <= DateTime.Now && s.EndDate >= DateTime.Now).FirstOrDefault();
        }
    }
}