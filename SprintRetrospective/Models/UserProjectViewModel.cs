﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace SprintRetrospective.Models
{
    public class UserProjectViewModel
    {
        public UserProjectViewModel() { }
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public List<User> Users { get; set; }
    }
}
