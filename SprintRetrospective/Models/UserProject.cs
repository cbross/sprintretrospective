﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace SprintRetrospective.Models
{
    public class UserProject
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int ProjectId { get; set; }

        public string UserId { get; set; }

        [ForeignKey("ProjectId")]
        public Project Project { get; set; } // generates FK
        [ForeignKey("UserId")]
        public User User { get; set; }

        [Required]
        public bool IsProjectOwner { get; set; }

        [Column(TypeName = "timestamp")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [MaxLength(8)]
        public byte[] Timer { get; set; }
    }
}