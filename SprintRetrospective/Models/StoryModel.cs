﻿using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class StoryModel
    {
        private AppDbContext _db;
        public StoryModel(AppDbContext ctx)
        {
            _db = ctx;
        }

        public Story Get(int id)
        {
            return _db.Stories.Where(s => s.Id == id).FirstOrDefault();
        }

        // Retrieves all stories for a given project that is in the backlog (stories that have yet to be completed)
        public List<Story> GetAllByProject(int projectId)
        {
            return _db.Stories.Where(s => s.ProjectId == projectId && s.Status != "Delivered").ToList<Story>();
        }

        public List<Task> GetAllTasks(int storyId)
        {
            return _db.Tasks.Where(t => t.StoryId == storyId).ToList<Task>();
        }

        public List<Story> GetAllBySprint(int sprintId)
        {
            var sprintStories = _db.SprintStories.Where(ss => ss.SprintId == sprintId).ToList();

            List<Story> stories = new List<Story>();
            foreach (SprintStory ss in sprintStories)
            {
                Story story = _db.Stories.Where(s => s.Id == ss.StoryId).FirstOrDefault<Story>();
                stories.Add(story);
            }
            return stories;
        }
    }
}