﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class BacklogViewModel
    {
        public BacklogViewModel() { }
        public Project Project { get; set; }
        public List<Story> Stories { get; set; }
        public List<UserStory> UserStories { get; set; }
        public List<Task> Tasks { get; set; }
        public Sprint CurrentSprint { get; set; }
        public List<Story> CurrentSprintStories { get; set; }
    }
}