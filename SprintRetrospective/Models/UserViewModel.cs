﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
namespace SprintRetrospective.Models
{
    public class UserViewModel
    {
        public UserViewModel() { }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public int Id { get; set; }
        public List<User> Users { get; set; }
        public IEnumerable<SelectListItem> GetUsers()
        {
            return Users.Select(user => new SelectListItem { Text = user.FirstName + " " + user.LastName, Value = user.Id.ToString() });
        }
    }
}