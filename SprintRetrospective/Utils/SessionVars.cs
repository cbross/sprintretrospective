﻿namespace SprintRetrospective.Utils
{
    public static class SessionVars
    {
        public const string LoginStatus = "";
        public const string User = "user";
        public const string Message = "message";
        public const string Error = "error";
    }
}
