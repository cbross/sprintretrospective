﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SprintRetrospective.Models;
using System.Collections.Generic;

namespace SprintRetrospective.Controllers
{
    public class ProjectController : Controller
    {
        AppDbContext _db;
        public ProjectController(AppDbContext context)
        {
            _db = context;
        }
        public IActionResult Index()
        {
            ProjectModel model = new ProjectModel(_db);
            ProjectViewModel viewModel = new ProjectViewModel();
            viewModel.Projects = model.GetAll();
            return View(viewModel);
        }

        public IActionResult Show(int id)
        {
            ProjectModel model = new ProjectModel(_db);
            ProjectViewModel viewModel = new ProjectViewModel();
            Project project = model.Get(id);
            viewModel.Id = project.Id;
            viewModel.ProjectName = project.Name;
            viewModel.StartDate = project.StartDate;
            viewModel.InitialVelocity = project.InitialVelocity;
            viewModel.PivotalTrackerLink = project.PivotalTrackerLink;
            viewModel.HoursPerStoryPoint = project.HoursPerStoryPoint;

            TeamModel tm = new TeamModel(_db);

            viewModel.Users = tm.GetUsers(project.TeamId);
            viewModel.Team = tm.Get(project.TeamId);
            return View(viewModel);
        }

        public IActionResult New()
        {
            ProjectModel model = new ProjectModel(_db);
            ProjectViewModel viewModel = new ProjectViewModel();

            TeamModel tm = new TeamModel(_db);
            viewModel.Teams = tm.GetAll();
            return View(viewModel);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Create(ProjectViewModel projectVm)
        {
            TeamModel tm = new TeamModel(_db);
            if (ModelState.IsValid)
            {
                Project project = new Project();

                project.Name = projectVm.ProjectName;
                project.PivotalTrackerLink = projectVm.PivotalTrackerLink;
                project.InitialVelocity = projectVm.InitialVelocity;
                projectVm.Team = tm.Get(projectVm.TeamId);
                project.Team = projectVm.Team;
                project.InitialVelocity = projectVm.InitialVelocity;
                project.StartDate = projectVm.StartDate;
                project.HoursPerStoryPoint = projectVm.HoursPerStoryPoint;
                _db.Add(project);

                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Show), new { id = project.Id });
            }
            else
            {
                projectVm.Teams = tm.GetAll();
                return View(nameof(New), projectVm);
            }
        }
    }
}