﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SprintRetrospective.Models;
using System.Collections.Generic;

namespace SprintRetrospective.Controllers
{
    public class BacklogController : Controller
    {
        AppDbContext _db;
        public BacklogController(AppDbContext context)
        {
            _db = context;
        }
        public IActionResult Show(int id)
        {
            BacklogViewModel backlogViewModel = new BacklogViewModel();
            StoryModel storyModel = new StoryModel(_db);
            SprintModel sprintModel = new SprintModel(_db);
            ProjectModel projectModel = new ProjectModel(_db);

            backlogViewModel.Stories = storyModel.GetAllByProject(id);
            backlogViewModel.Project = projectModel.Get(id);
            backlogViewModel.CurrentSprint = sprintModel.Get(id);
            if (backlogViewModel.CurrentSprint != null)
            {
                backlogViewModel.CurrentSprintStories = storyModel.GetAllBySprint(backlogViewModel.CurrentSprint.Id);

                foreach (Story storyInSprint in backlogViewModel.CurrentSprintStories)
                    backlogViewModel.Stories.Remove(storyInSprint);
            }

            return View(backlogViewModel);
        }

        public IActionResult Details(int id)
        {
            StoryViewModel storyViewModel = new StoryViewModel();
            StoryModel storyModel = new StoryModel(_db);
            Story story = storyModel.Get(id);

            storyViewModel.Id = story.Id;
            storyViewModel.Name = story.Name;
            storyViewModel.Description = story.Description;
            storyViewModel.RelativeEstimate = story.RelativeEstimate;
            storyViewModel.HoursWorked = story.HoursWorked;
            storyViewModel.Tasks = storyModel.GetAllTasks(id);
            storyViewModel.ProjectId = story.ProjectId;
            storyViewModel.Status = story.Status;
            return View(storyViewModel);
        }

        public IActionResult New(int id)
        {
            StoryViewModel storyViewModel = new StoryViewModel();
            storyViewModel.ProjectId = id;

            return View(storyViewModel);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Create(StoryViewModel storyVm)
        {
            if (ModelState.IsValid)
            {
                Story story = new Story();

                story.Name = storyVm.Name;
                story.Description = storyVm.Description;
                story.RelativeEstimate = storyVm.RelativeEstimate;
                story.ProjectId = storyVm.ProjectId;
                _db.Add(story);
                story.UserId = "";

                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Show), new { id = story.ProjectId });
            }
            else
            {
                return View(nameof(New), storyVm);
            }
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Update(StoryViewModel storyVm)
        {
            if (ModelState.IsValid)
            {
                Story story = new Story();

                story.Id = storyVm.Id;
                story.Name = storyVm.Name;
                story.Description = storyVm.Description;
                story.RelativeEstimate = storyVm.RelativeEstimate;
                story.ProjectId = storyVm.ProjectId;
                story.UserId = "";
                story.Status = storyVm.Status;
                _db.Update(story);

                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Show), new { id = story.ProjectId });
            }
            else
            {
                return View(nameof(New), storyVm);
            }
        }
    }
}