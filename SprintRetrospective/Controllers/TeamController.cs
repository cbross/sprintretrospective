﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SprintRetrospective.Models;

namespace SprintRetrospective.Controllers
{
    public class TeamController : Controller
    {
        AppDbContext _db;

        public TeamController(AppDbContext context)
        {
            _db = context;
        }

        public IActionResult Index()
        {
            UserModel userModel = new UserModel(_db);
            TeamViewModel viewModel = new TeamViewModel();
            viewModel.Teams = userModel.GetAllTeams();

            return View(viewModel);
        }

        public IActionResult Show(int id)
        {
            TeamModel model = new TeamModel(_db);
            TeamViewModel viewModel = new TeamViewModel();
            Team team = model.Get(id);
            viewModel.Id = team.Id;
            viewModel.Name = team.Name;
            viewModel.Users = model.GetUsers(team.Id);
            return View(viewModel);
        }

        public IActionResult New()
        {
            TeamModel model = new TeamModel(_db);
            TeamViewModel viewModel = new TeamViewModel();

            return View(viewModel);
        }

        public IActionResult Add(int id)
        {
            UserModel um = new UserModel(_db);
            TeamModel tm = new TeamModel(_db);
            TeamViewModel teamVm = new TeamViewModel();
            Team team = tm.Get(id);
            teamVm.Id = team.Id;
            teamVm.Name = team.Name;
            teamVm.Users = tm.GetUsers(id);

            teamVm.AllUsers = um.GetAll();

            foreach(var u in teamVm.Users)
            {
                teamVm.AllUsers.Remove(u);
            }

            return View(teamVm);
        }

        public IActionResult Remove(int id)
        {
            UserModel um = new UserModel(_db);
            TeamModel tm = new TeamModel(_db);
            TeamViewModel teamVm = new TeamViewModel();
            Team team = tm.Get(id);
            teamVm.Id = team.Id;
            teamVm.Name = team.Name;
            teamVm.Users = tm.GetUsers(id);

            return View(teamVm);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> AddMember(string id)
        {
            TeamModel tm = new TeamModel(_db);
            int teamId = Int32.Parse(HttpContext.Request.Query["teamid"]);
            Team team = tm.Get(teamId);

            // Get information for current team;
            TeamViewModel teamVm = new TeamViewModel();
            teamVm.Id = team.Id;
            teamVm.Name = team.Name;
            UserModel um = new UserModel(_db);
            User newUser = um.Get(id);

            UserTeam newUserTeam = new UserTeam { Team = team, User = newUser };
            _db.Add(newUserTeam);

            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Show), teamVm);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> RemoveMember(string id)
        {
            TeamModel tm = new TeamModel(_db);
            int teamId = Int32.Parse(HttpContext.Request.Query["teamid"]);
            Team team = tm.Get(teamId);

            // Get information for current team;
            TeamViewModel teamVm = new TeamViewModel();
            teamVm.Id = team.Id;
            teamVm.Name = team.Name;
            UserModel um = new UserModel(_db);
            User removeUser = um.Get(id);

            UserTeam newUserTeam = tm.GetUserTeam(team, removeUser, id);
            _db.Remove(newUserTeam);

            await _db.SaveChangesAsync();
            return RedirectToAction(nameof(Show), teamVm);
        }

        [HttpPost]
        public async System.Threading.Tasks.Task<IActionResult> Create(TeamViewModel teamVm)
        {
            TeamModel tm = new TeamModel(_db);
            if (ModelState.IsValid)
            {
                Team team = new Team();

                team.Name = teamVm.Name;
                _db.Add(team);

                await _db.SaveChangesAsync();
                return RedirectToAction(nameof(Show), new { id = team.Id });
            }
            else
            {
                teamVm.Teams = tm.GetAll();
                return View(nameof(New), teamVm);
            }
        }
    }
}