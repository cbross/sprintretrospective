﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using SprintRetrospective.Models;
using SprintRetrospective.Utils;

namespace SprintRetrospective.Controllers
{
    public class LoginController : Controller
    {
        UserManager<User> _usrMgr;
        SignInManager<User> _signInMgr;
        public LoginController(UserManager<User> userManager,
        SignInManager<User> signInManager)
        {
            _usrMgr = userManager;
            _signInMgr = signInManager;
        }

        // GET: Login
        [AllowAnonymous]
        public ActionResult Index(string ReturnUrl = null)
        {
            if (HttpContext.Session.Get(SessionVars.LoginStatus) == null || Convert.ToString(HttpContext.Session.Get(SessionVars.LoginStatus)) == "")
            {
                HttpContext.Session.SetString(SessionVars.LoginStatus, "not logged in");
            }
            if (Convert.ToString(HttpContext.Session.Get(SessionVars.LoginStatus)) == "not logged in")
            {
                HttpContext.Session.SetString(SessionVars.Message, "Please login to access full features");
            }
            ViewBag.status = HttpContext.Session.GetString(SessionVars.LoginStatus);
            ViewBag.message = HttpContext.Session.GetString(SessionVars.Message);
            ViewBag.ReturnUrl = ReturnUrl;
            return View();
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var result = await _signInMgr.PasswordSignInAsync(model.Username, model.Password, false, false);
                if (result.Succeeded)
                {
                    HttpContext.Session.SetString(SessionVars.User, model.Username);
                    HttpContext.Session.SetString(SessionVars.LoginStatus, "Logged in as " + model.Username);
                    HttpContext.Session.SetString(SessionVars.Message, "Welcome " + model.Username + "! Visit your Dashboard to view your projects.");
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    HttpContext.Session.SetString(SessionVars.Error, "Login Failed. Please try again.");
                    ViewBag.status = HttpContext.Session.GetString(SessionVars.LoginStatus);
                    ViewBag.error = HttpContext.Session.GetString(SessionVars.Error);
                    return View("Index");
                }
            }
            // If we got this far, something failed, redisplay form
            return RedirectToLocal(returnUrl);
        }

        public async Task<IActionResult> Logoff()
        {
            await _signInMgr.SignOutAsync();
            HttpContext.Session.Clear();
            HttpContext.Session.SetString(SessionVars.LoginStatus, "not logged in");
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
        }
    }
}