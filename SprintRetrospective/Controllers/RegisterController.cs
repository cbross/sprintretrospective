﻿using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SprintRetrospective.Models;
using SprintRetrospective.Utils;

namespace SprintRetrospective.Controllers
{
    public class RegisterController : Controller
    {
        UserManager<User> _usrMgr;
        SignInManager<User> _signInMgr;
        public RegisterController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _usrMgr = userManager;
            _signInMgr = signInManager;
        }

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View();
        }

        // POST:/Register/Register
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new User { UserName = model.Username, NormalizedUserName = model.Username.ToLower(), Email = model.Email, NormalizedEmail = model.Email.ToLower(), FirstName = model.Firstname, LastName = model.Lastname };
                var result = await _usrMgr.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    await _signInMgr.PasswordSignInAsync(user.UserName, model.Password, false, false);
                    HttpContext.Session.SetString(SessionVars.LoginStatus, "logged on as " + user.Email);
                    HttpContext.Session.SetString(SessionVars.User, user.UserName);
                    HttpContext.Session.SetString(SessionVars.Message, "Registered, logged on as " + user.Email);
                }
                else
                {
                    StringBuilder errorMsg = new StringBuilder();
                    errorMsg.Append("Registration Failed <br />");
                    foreach (var error in result.Errors)
                    {
                        errorMsg.Append(error.Description + "<br />");
                    }
                    ViewBag.message = errorMsg.ToString();
                    return View("Index");
                }
            }
            return Redirect("/Home");
        }
    }
}