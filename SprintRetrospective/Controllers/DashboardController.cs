﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SprintRetrospective.Models;

namespace SprintRetrospective.Controllers
{
    public class DashboardController : Controller
    {
        AppDbContext _db;
        public DashboardController(AppDbContext context)
        {
            _db = context;
        }
        // Index page
        public IActionResult Index()
        {
            return View();
        }
    }
}