﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SprintRetrospective.Models;
using Microsoft.AspNetCore.Identity;

namespace SprintRetrospective.Data
{
    public class DbInitializer
    {
        public static void Initialize(AppDbContext context, UserManager<User> userManager)
        {
            UserManager<User> _usrMgr = userManager;
            context.Database.EnsureCreated();

            // ADD USERS
            if (context.Users.Any() == false)
            {
                var users = new User[]
                {
                new User{UserName="candisbross",Email="cbross@example.com",FirstName="Cas",LastName="Bross"},
                new User{UserName="ewilliams",Email="ewilliams@example.com",FirstName="Elizabeth",LastName="Williams"},
                new User{UserName="mbrown",Email="mbrown@example.com",FirstName="Michael",LastName="Brown"},
                new User{UserName="mbach",Email="mbachinger@example.com",FirstName="Marty",LastName="Bachinger"},
                new User{UserName="sweiche",Email="sweiche@example.com",FirstName="Shamus",LastName="Weiche"}
                };
                foreach (User user in users)
                {
                    var result = _usrMgr.CreateAsync(user, "Password123").Result;

                    Console.WriteLine("User created");
                }
            }
            context.SaveChanges();

            // ADD TEAMS
            if (context.Teams.Any() == false)
            {
                // Add projects
                var teams = new Team[]
                {
                new Team{Name="Red"},
                new Team{Name="Orange"},
                new Team{Name="Blue"},
                new Team{Name="Purple"},
                };
                foreach (Team team in teams)
                {
                    context.Teams.Add(team);
                }
            }
            context.SaveChanges();

            // ADD PROJECTS
            if (context.Projects.Any() == false)
            {
                // Add projects
                var projects = new Project[]
                {
                new Project{Name="Fitness Guru",StartDate=DateTime.Now,InitialVelocity=12,HoursPerStoryPoint=3,TeamId=1,PivotalTrackerLink="https://www.pivotaltracker.com/n/projects/2315810"},
                new Project{Name="Software Reviews",StartDate=DateTime.Now,InitialVelocity=21,HoursPerStoryPoint=3,TeamId=1,PivotalTrackerLink="https://www.pivotaltracker.com/n/projects/2315810"},
                new Project{Name="College Enrollment",StartDate=DateTime.Now,InitialVelocity=15,HoursPerStoryPoint=3,TeamId=1,PivotalTrackerLink="https://www.pivotaltracker.com/n/projects/2315810"},
                };
                foreach (Project project in projects)
                {
                    context.Projects.Add(project);
                }
            }
            context.SaveChanges();

            // ADD USERPROJECTS
            if (context.UserProjects.Any() == false)
            {
                List<User> users = (from user in context.Users select user).ToList<User>();
                List<Project> projects = (from pro in context.Projects select pro).ToList<Project>();
                // Add users to projects
                var userProjects = new UserProject[]
                {
                    new UserProject{User=users.ElementAt(0),ProjectId=projects.ElementAt(0).Id,IsProjectOwner=true},
                    new UserProject{User=users.ElementAt(2),ProjectId=projects.ElementAt(1).Id,IsProjectOwner=true},
                    new UserProject{User=users.ElementAt(3),ProjectId=projects.ElementAt(2).Id,IsProjectOwner=true}
                };
                foreach (UserProject userProject in userProjects)
                {
                    context.UserProjects.Add(userProject);
                }
            }
            context.SaveChanges();

            // ADD USERTEAMS
            if (context.UserTeams.Any() == false)
            {
                List<User> users = (from user in context.Users select user).ToList<User>();
                List<Team> teams = (from team in context.Teams select team).ToList<Team>();
                // Add users to teams
                var userTeams = new UserTeam[]
                {
                    new UserTeam{Team=teams.ElementAt(0),User=users.ElementAt(0)},
                    new UserTeam{Team=teams.ElementAt(0),User=users.ElementAt(1)},
                    new UserTeam{Team=teams.ElementAt(0),User=users.ElementAt(2)},
                    new UserTeam{Team=teams.ElementAt(1),User=users.ElementAt(2)},
                    new UserTeam{Team=teams.ElementAt(1),User=users.ElementAt(3)},
                    new UserTeam{Team=teams.ElementAt(1),User=users.ElementAt(4)},
                    new UserTeam{Team=teams.ElementAt(2),User=users.ElementAt(1)},
                    new UserTeam{Team=teams.ElementAt(2),User=users.ElementAt(2)},
                    new UserTeam{Team=teams.ElementAt(2),User=users.ElementAt(4)},
                };
                foreach (UserTeam ut in userTeams)
                {
                    context.UserTeams.Add(ut);
                }
            }
            context.SaveChanges();


            // ADD STORIES
            if (context.Stories.Any() == false)
            {
                List<Project> projects = (from pro in context.Projects select pro).ToList<Project>();
                List<User> users = (from user in context.Users select user).ToList<User>();

                var stories = new Story[]
                {
                    new Story{
                        Name ="As a user I want to sign up for the service", Description="Add a sign up and login page", RelativeEstimate=2,
                        HoursWorked=0, ProjectId=projects.ElementAt(0).Id, UserId=users.ElementAt(0).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to update my profile and account", Description="Add a profile page that allows you to edit user information", RelativeEstimate=2,
                        HoursWorked=1, ProjectId=projects.ElementAt(0).Id, UserId=users.ElementAt(0).Id,
                        Status="Started"},
                    new Story{
                        Name ="As a user I want to sign up for a trainer plan", Description="Add a page to sign up for trainer services. Add trainers to the site. Add profile pages for all trainers.", RelativeEstimate=8,
                        HoursWorked=0, ProjectId=projects.ElementAt(0).Id, UserId=users.ElementAt(0).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to sign up for the service", Description="Add a sign up and login page", RelativeEstimate=3,
                        HoursWorked=0, ProjectId=projects.ElementAt(1).Id, UserId=users.ElementAt(2).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to rate software", Description="Testing Description", RelativeEstimate=8,
                        HoursWorked=0, ProjectId=projects.ElementAt(1).Id, UserId=users.ElementAt(2).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to view rated software", Description="Testing Description", RelativeEstimate=3,
                        HoursWorked=0, ProjectId=projects.ElementAt(1).Id, UserId=users.ElementAt(2).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to sign up for the service", Description="Testing Description", RelativeEstimate=1,
                        HoursWorked=3, ProjectId=projects.ElementAt(2).Id, UserId=users.ElementAt(1).Id,
                        Status="Delivered"},
                    new Story{
                        Name ="As a user I want to sign up for the service", Description="Testing Description", RelativeEstimate=2,
                        HoursWorked=0, ProjectId=projects.ElementAt(2).Id, UserId=users.ElementAt(1).Id,
                        Status="Planned"},
                    new Story{
                        Name ="As a user I want to sign up for the service", Description="Testing Description", RelativeEstimate=3,
                        HoursWorked=0, ProjectId=projects.ElementAt(2).Id, UserId=users.ElementAt(1).Id,
                        Status="Planned"},

                };

                foreach (Story sto in stories)
                {
                    context.Stories.Add(sto);
                }
            }
            context.SaveChanges();

            // ADD USERSTORIES
            if (context.UserStories.Any() == false)
            {
                List<User> users = (from user in context.Users select user).ToList<User>();
                List<Story> stories = (from story in context.Stories select story).ToList<Story>();

                var userStories = new UserStory[]
                {
                    new UserStory{Story=stories.ElementAt(0),User=users.ElementAt(0)},
                    new UserStory{Story=stories.ElementAt(1),User=users.ElementAt(1)},
                    new UserStory{Story=stories.ElementAt(2),User=users.ElementAt(2)},
                    new UserStory{Story=stories.ElementAt(3),User=users.ElementAt(2)},
                    new UserStory{Story=stories.ElementAt(4),User=users.ElementAt(3)},
                    new UserStory{Story=stories.ElementAt(5),User=users.ElementAt(4)},
                    new UserStory{Story=stories.ElementAt(6),User=users.ElementAt(1)},
                    new UserStory{Story=stories.ElementAt(7),User=users.ElementAt(2)},
                    new UserStory{Story=stories.ElementAt(8),User=users.ElementAt(4)},
                };

                foreach (UserStory us in userStories)
                {
                    context.UserStories.Add(us);
                }
            }
            context.SaveChanges();

            // ADD SPRINTS
            if (context.Sprints.Any() == false)
            {
                List<Project> projects = (from project in context.Projects select project).ToList<Project>();

                var sprints = new Sprint[]
                {
                    new Sprint{
                        StartDate = new DateTime(2019, 4, 8),
                        EndDate =   new DateTime(2019, 4, 21),
                        ProjectId = projects.ElementAt(0).Id
                    },
                    new Sprint{
                        StartDate = new DateTime(2019, 4, 8),
                        EndDate =   new DateTime(2019, 4, 21),
                        ProjectId = projects.ElementAt(1).Id
                    },
                    new Sprint{
                        StartDate = new DateTime(2019, 4, 8),
                        EndDate =   new DateTime(2019, 4, 21),
                        ProjectId = projects.ElementAt(2).Id
                    },
                };

                foreach (Sprint sprint in sprints)
                {
                    context.Sprints.Add(sprint);
                }
            }
            context.SaveChanges();

            // ADD SPRINTSTORIES
            if (context.SprintStories.Any() == false)
            {
                List<Story> stories = (from story in context.Stories select story).ToList<Story>();
                List<Sprint> sprints = (from sprint in context.Sprints select sprint).ToList<Sprint>();

                var sprintStories = new SprintStory[]
                {
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(0).Id,
                        StoryId = stories.ElementAt(0).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(0).Id,
                        StoryId = stories.ElementAt(1).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(0).Id,
                        StoryId = stories.ElementAt(2).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(1).Id,
                        StoryId = stories.ElementAt(3).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(1).Id,
                        StoryId = stories.ElementAt(4).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(1).Id,
                        StoryId = stories.ElementAt(5).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(2).Id,
                        StoryId = stories.ElementAt(6).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(2).Id,
                        StoryId = stories.ElementAt(7).Id
                    },
                    new SprintStory
                    {
                        SprintId = sprints.ElementAt(2).Id,
                        StoryId = stories.ElementAt(8).Id
                    },
                };

                foreach (var sprStr in sprintStories)
                {
                    context.SprintStories.Add(sprStr);
                }
            }
            context.SaveChanges();
        }
    }
}
